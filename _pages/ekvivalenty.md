---
title: Tabulka ekvivalentních aplikací MS Windows a Linux
description: Proč zvolit jako svůj operační systém Linux a jak zjistit který software odpovídá mým požadavkům.
permalink: /ekvivalenty/
redirect_from:
 - ekvivalenty.html
 - prevod-dat.html
layout: default
showtitle: true
---
Přesvědčte se, že svobodné multiplatformní softwarové nástroje vám mohou poskytnout to, co potřebujete, a zdarma!

Přechod na Linux vám usnadní, pokud už na Windows používáte některé multiplatformní aplikace. To jsou aplikace, které fungují na více operačních systémech a většinou umožňují i snadný přenos dat a nastavení. Také si u nich po změně systému nemusíte zvykat na nové uživatelské rozhraní nebo hledat potřebné funkce.

Zajímavé multiplatformní aplikace jsou v seznamu níže <span class="mp">označeny takto</span>.

* TOC
{:toc}

## Zkrácený seznam nejběžnějších aplikací
Některé z těchto programů nebo jiné jejich alternativy už bývají výchozí součástí většiny distribucí.

| MS Windows | GNU/Linux |
| --- |
| MS Internet Explorer, Edge | [Mozilla Firefox](https://www.mozilla.cz/produkty/firefox/){:.mp} |
| Pošta, Outlook Express, Outlook | [Mozilla Thunderbird](https://www.mozilla.cz/produkty/thunderbird/){:.mp}, [Evolution](https://wiki.gnome.org/Apps/Evolution), nebo dílčí aplikace v distribuci |
| ICQ, MSN | [Pidgin](https://pidgin.im/){:.mp}, [Kopete](https://userbase.kde.org/Kopete) |
| Skype | [Skype pro Linux](https://www.skype.com/){:.mp} [Prop] |
| Firewall | standardně součástí distribuce |
| Antivirus, Antispyware | standardně není potřeba |
| Průzkumník | součástí prostředí, např. [Konqueror](https://konqueror.org/) (KDE) a [Nautilus](https://wiki.gnome.org/action/show/Apps/Files) (GNOME) |
| Total Commander | [Double Commander](https://doublecmd.sourceforge.io/){:.mp} [Krusader](https://krusader.org/) |
| WinZip | součástí prostředí |
| Poznámkový blok, Notepad | součástí prostředí, např. [Kate](https://www.kde.org/applications/utilities/kate/) (KDE) a [Gedit](https://wiki.gnome.org/Apps/Gedit) (GNOME), nebo [Atom](https://atom.io/){:.mp} |
| MS Office | [LibreOffice](https://www.libreoffice.org/){:.mp} |
| Adobe Reader | součástí prostředí, např. [Okular](https://okular.kde.org/) (KDE) a [Evince](https://wiki.gnome.org/Apps/Evince) (GNOME) |
| WinAmp, iTunes | součástí prostředí, např. [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox) (GNOME) a [Audacious](https://audacious-media-player.org/){:.mp}, nebo [Amarok](https://amarok.kde.org/){:.mp} |
| Media Player | součástí prostředí, např. [Totem](https://wiki.gnome.org/Apps/Videos/) (GNOME), nebo [VLC media player](https://www.videolan.org/){:.mp} |
| ACDSee, Prohlížeč fotografií | součástí distribuce |
| Photoshop, Paint Shop | [GIMP](https://www.gimp.org/){:.mp}, [Krita](https://krita.org/){:.mp}, [Inkscape](https://inkscape.org/){:.mp} |
{:.table .table-bordered .table-hover}

## Rozšířený seznam ekvivalentů
Většina programů pro MS Windows se snaží vycházet z principu "vše v jednom" - tedy každý vývojář přidává všechny vlastnosti do svého vlastního produktu.

Ideou Linuxu je mít pouze jeden program, který dělá danou věc, ale dělá ji dobře. Proto se dají programy v Linuxu přirovnat spíše k Legu - např. kontrola pravopisu je použita stejná nejen v textovém editoru, ale i v poštovním programu atd. Typické je také to, že pokud existuje kvalitní aplikace pracující z příkazové řádky, aplikace s grafickým rozhraním fungují jako její nadstavba, která tento program využívá a poskytuje uživatelsky přívětivější rozhraní.

Alternativ je ale v prostředí Linuxu velmi mnoho, takže pokud si ze seznamu níže nevyberete, můžete k nalezení dalších použít svůj oblíbený vyhledávač.

### Internet
{:#internet}

| Popis programu | MS Windows | GNU/Linux |
| --- |
| Webový prohlížeč | Internet Explorer <br> Edge <br> [Mozilla Firefox](https://www.mozilla.cz/produkty/firefox/){:.mp} <br> [Chrome](https://www.google.com/chrome){:.mp} <br> [Opera](https://www.opera.com/){:.mp} <br> [Vivaldi](https://vivaldi.net/){:.mp} | [Mozilla Firefox](https://www.mozilla.cz/produkty/firefox/){:.mp} <br> [Chromium](https://www.chromium.org/){:.mp} <br> [Opera](https://www.opera.com/){:.mp} [Prop] <br> [Vivaldi](https://vivaldi.net/){:.mp} [Prop] <br> [GNOME Web](https://wiki.gnome.org/action/show/Apps/Web) <br> [Konqueror](https://www.konqueror.org/) <br> [Midori](http://twotoasts.de/index.php/midori/) |
| Blokování reklam | doplňky pro Firefox/Chrome | stejné doplňky pro Firefox/Chrome |
| Poštovní programy | Outlook Express <br> Pošta <br> [Mozilla Thunderbird](https://www.mozilla.cz/stahnout/thunderbird/){:.mp} <br> The Bat <br> Eudora | [Mozilla Thunderbird](https://www.mozilla.cz/stahnout/thunderbird/){:.mp} <br> [Evolution](https://wiki.gnome.org/Apps/Evolution) <br> [Geary](https://wiki.gnome.org/Apps/Geary) <br> [Claws Mail](https://www.claws-mail.org/) <br> [Kontact](https://userbase.kde.org/Kontact) ([Kmail](https://userbase.kde.org/Kontact/cs)) <br> [Sylpheed](https://sylpheed.sraoss.jp/en/) |
| Stahování souborů | doplňky pro Firefox/Chrome <br> Free Download Manager <br> Internet Download Manager <br> [FreeRapid Downloader](https://wordrider.net/freerapid/){:.mp} <br> [JDownloader](https://jdownloader.org/){:.mp} | stejné doplňky pro Firefox/Chrome <br> [FreeRapid Downloader](https://wordrider.net/freerapid/){:.mp} <br> [Kget](https://www.kde.org/applications/internet/kget/) <br> [JDownloader](https://jdownloader.org/){:.mp} |
| Synchronizace souborů | [Dropbox](https://www.dropbox.com/){:.mp} <br> Google Drive <br> MS OneDrive <br> [Nextcloud](https://nextcloud.com/){:.mp} | [Dropbox](https://www.dropbox.com/){:.mp} [Prop] <br> GNOME Účty on-line + Nautilus (Google Drive) <br> [OneDrive, for Linux](https://skilion.github.io/onedrive/) <br> [Nextcloud](https://nextcloud.com/){:.mp} |
| FTP klienti | [FileZilla](https://filezilla-project.org/){:.mp} <br> WinSCP <br> Bullet Proof FTP <br> SmartFTP <br> CuteFTP <br> WSFTP | [FileZilla](https://filezilla-project.org/){:.mp} <br> [KFTPgrabber](https://www.kde.org/applications/internet/kftpgrabber/) <br> [Nautilus](https://wiki.gnome.org/action/show/Apps/Files) <br> [Konqueror](https://konqueror.org/) |
| Instant Messaging klienti | Facebook Messenger (web) <br> Google Hangouts (web) <br> [Skype](https://www.skype.com/){:.mp} <br> [Pidgin](https://pidgin.im/){:.mp} <br> Miranda <br> [Telegram](https://telegram.org/){:.mp} | Facebook Messenger (web) <br> Google Hangouts (web) <br> [Skype](https://www.skype.com/){:.mp} [Prop] <br> [Pidgin](https://pidgin.im/){:.mp} <br> [Kopete](https://userbase.kde.org/Kopete) <br> [Empathy](https://wiki.gnome.org/action/show/Apps/Empathy) <br> [Telegram](https://telegram.org/){:.mp} [Prop] |
| Video/audio komunikace | Facebook Messenger (web) <br> Google Hangouts (web) <br> Appear.in (web) <br> [Skype](https://www.skype.com/){:.mp} | Facebook Messenger (web) <br> Google Hangouts (web) <br> Appear.in (web) <br> [Skype](https://www.skype.com/){:.mp} [Prop] <br> [Empathy](https://wiki.gnome.org/action/show/Apps/Empathy) |
| Firewall | AVG Internet Security <br> Avast Internet Security <br> Eset Internet Security <br> Norton Internet Security | standardně součástí distribuce <br> iptables (konsole) <br> ufw (konsole) <br> [Gufw](https://gufw.org/) |
| Peer-to-peer klienti (sdílení souborů) | BitTorrent <br> [Transmission](https://transmissionbt.com/){:.mp} (Bittorrent) <br> [uTorrent](https://www.utorrent.com/desktop/){:.mp} <br> [Deluge](https://deluge-torrent.org/){:.mp} (Bittorrent) <br> eMule | [Transmission](https://transmissionbt.com/){:.mp} (Bittorrent) <br> [uTorrent](https://www.utorrent.com/desktop/){:.mp} [Prop] <br> [qBittorrent](https://www.qbittorrent.org/){:.mp} <br> [Deluge](https://deluge-torrent.org/){:.mp} (Bittorrent) <br> [KTorrent](https://www.kde.org/applications/internet/ktorrent/) <br> [Gtk-Gnutella](https://gtk-gnutella.sourceforge.net/) <br> [GNUnet](https://gnunet.org/) |
| Vzdálená správa | [RealVNC](https://www.realvnc.com/){:.mp} <br> [TeamViewer](https://www.teamviewer.com/){:.mp} <br> LogMeIn <br> Radmin | [RealVNC](https://www.realvnc.com/){:.mp} <br> [TeamViewer](https://www.teamviewer.com/){:.mp} [Prop] <br> [ssh](https://www.openssh.com/) <br> [Krfb](https://www.kde.org/applications/system/krfb/) <br> [Vinagre](https://www.kde.org/applications/system/krfb/) <br> [Rdesktop](http://www.rdesktop.org/) |
| Prohlížení sousedních počítačů | součást systému | [Samba](https://www.samba.org/) <br> [Konqueror](https://konqueror.org/) <br> [Nautilus](https://wiki.gnome.org/action/show/Apps/Files) |
{:.table .table-bordered .table-hover}

### Kancelářské nástroje
{:#kancelar}

| Popis programu | MS Windows | GNU/Linux |
| --- |
| Kancelářské balíky | MS Office <br> [LibreOffice](https://www.libreoffice.org/){:.mp} <br> 602Software | [LibreOffice](https://www.libreoffice.org/){:.mp} <br> [ONLYOFFICE](https://www.onlyoffice.com/){:.mp} <br> [Calligra](https://www.calligra.org/){:.mp} <br> [GNOME Office](https://wiki.gnome.org/Attic/GnomeOffice) <br> [WPS Office](https://www.wps.com/linux){:.mp} [Prop] |
| Editory dokumentů | MS Word <br> [LibreOffice Writer](https://www.libreoffice.org/discover/writer/){:.mp} <br> 602Text | [LibreOffice Writer](https://www.libreoffice.org/discover/writer/){:.mp} <br> [ONLYOFFICE](https://www.onlyoffice.com/){:.mp} <br> [Calligra Words](https://www.calligra.org/words/){:.mp} <br> [Abiword](https://www.abisource.com/){:.mp} <br> [LaTeX](https://www.latex-project.org/){:.mp} <br> [LyX](https://www.lyx.org/){:.mp} <br> [Kile](https://kile.sourceforge.io/) |
| Tabulkové procesory | MS Excel <br> [LibreOffice Calc](https://www.libreoffice.org/discover/calc/){:.mp} <br> 602Tab | [LibreOffice Calc](https://www.libreoffice.org/discover/calc/){:.mp} <br> [ONLYOFFICE](https://www.onlyoffice.com/){:.mp} <br> [Calligra Sheets](https://www.calligra.org/sheets/){:.mp} <br> [Gnumeric](http://www.gnumeric.org/) |
| Vytváření prezentací | MS PowerPoint <br> [LibreOffice Impress](https://www.libreoffice.org/discover/impress/){:.mp} | [LibreOffice Impress](https://www.libreoffice.org/discover/impress/){:.mp} <br> [ONLYOFFICE](https://www.onlyoffice.com/){:.mp} <br> [Calligra Stage](https://www.calligra.org/stage/){:.mp} |
| Správa osobních financí | Microsoft Money <br> Quicken <br> [Moneydance](https://moneydance.com/){:.mp} | [GNUcash](https://www.gnucash.org/){:.mp} <br> [Kmymoney](https://kmymoney.org/){:.mp} <br> [Grisbi](https://sourceforge.net/projects/grisbi/){:.mp} <br> [Moneydance](https://moneydance.com/){:.mp} [Prop] |
| Malé DTP nástroje | MS Publisher | [Scribus](https://www.scribus.net/){:.mp} |
{:.table .table-bordered .table-hover}

### Správa souborů
{:#ssouboru}

| Popis programu | MS Windows | GNU/Linux |
| --- |
| Správci souborů | Total Commander (původně Windows Commander) <br> FAR Manager | [Krusader](https://krusader.org/) <br> [Double Commander](https://doublecmd.sourceforge.io/){:.mp} <br> [GNOME Commander](https://gcmd.github.io/) <br> [muCommander](https://www.mucommander.com/){:.mp} <br> [Midnight Commander](https://midnight-commander.org/) |
| Komprimace souborů | [7-Zip](https://www.7-zip.org/){:.mp} <br> WinRar <br> WinZip <br> WinACE | [p7zip](https://www.7-zip.org/){:.mp} <br> [Ark](https://www.kde.org/applications/utilities/ark/) <br> [FileRoller](https://wiki.gnome.org/Apps/FileRoller) <br> [PeaZip](https://peazip.github.io/){:.mp} |
{:.table .table-bordered .table-hover}

### Textové editory
{:#teditory}

| Popis programu | MS Windows | GNU/Linux |
| --- |
| Textové editory | Notepad <br> WordPad <br> PSPad <br> Notepad++ | [Kate](https://kate-editor.org/) <br> [Gedit](https://wiki.gnome.org/Apps/Gedit) <br> [Vim](https://www.vim.org/) <br> [Emacs](https://www.gnu.org/software/emacs/) <br> [Scribes](https://scribes.sourceforge.net/) |
{:.table .table-bordered .table-hover}

### Práce s PDF
{:#ppdf}

| Popis programu | MS Windows | GNU/Linux |
| --- |
| Prohlížení PDF | Adobe Reader <br> [Foxit Reader](https://www.foxitsoftware.com/pdf-reader/){:.mp} <br> [GSView](https://gsview.com/){:.mp} | [Foxit Reader](https://www.foxitsoftware.com/pdf-reader/){:.mp} [Prop] <br> [Evince](https://wiki.gnome.org/Apps/Evince) <br> [Okular](https://okular.kde.org/) <br> [Kpdf](https://kpdf.kde.org/) <br> [Xpdf](https://www.xpdfreader.com/) <br> [GSView](https://gsview.com/){:.mp} |
| Vytváření PDF | Adobe Acrobat <br> [GSView](https://gsview.com/){:.mp} <br> [Ghostscript](https://www.ghostscript.com/){:.mp} <br> [LibreOffice](https://www.libreoffice.org/){:.mp} (export do PDF) | [pdfTeX](https://www.tug.org/applications/pdftex/) <br> [Ghostscript](https://www.ghostscript.com/){:.mp} <br> [GNOME Ghostview](https://directory.fsf.org/wiki/GGv) <br> [GSView](https://gsview.com/){:.mp} <br> [LibreOffice](https://www.libreoffice.org/){:.mp} (export do PDF) |
{:.table .table-bordered .table-hover}

### Hudba, CD a mp3
{:#hudba}

| Popis programu | MS Windows | GNU/Linux |
| --- |
| Přehrávač hudby a mp3 | iTunes <br> Windows Media Player <br> Winamp <br> MediaMonkey <br> Real Player | [Amarok](https://amarok.kde.org/){:.mp} <br> [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox) <br> [Banshee](https://bansheemediaplayer.github.io/){:.mp} <br> [Clementine](https://www.clementine-player.org/){:.mp} <br> [Audacious](https://audacious-media-player.org/){:.mp} <br> [Kaffeine](https://www.kde.org/applications/multimedia/kaffeine/) |
| Programy pro vypalování CD/DVD | Nero <br> CDBurnerXP | [K3b](https://userbase.kde.org/K3b) <br> [Brasero](https://wiki.gnome.org/Apps/Brasero) |
| Audio editory | [Audacity](https://www.audacityteam.org/){:.mp} <br> SoundForge <br> Cooledit <br> AVS Audio Editor | [Audacity](https://www.audacityteam.org/){:.mp} <br> [Ardour](https://ardour.org/){:.mp} <br> [Qtractor](https://qtractor.sourceforge.io/){:.mp} <br> [ocenaudio](https://www.ocenaudio.com/){:.mp} |
| Editace ID3 tagů | Mp3tag <br> ID3 Tag Editor <br> MediaMonkey <br> Foobar2000 | [EasyTAG](https://wiki.gnome.org/Apps/EasyTAG) <br> [Amarok](https://amarok.kde.org/){:.mp} <br> [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox) <br> [Banshee](https://bansheemediaplayer.github.io/){:.mp} |
{:.table .table-bordered .table-hover}

### Grafika a obrázky
{:#grafika}

| Popis programu | MS Windows | GNU/Linux |
| --- |
| Prohlížeče obrázků | [Xnview](https://www.xnview.com/){:.mp} <br> ACDSee <br> IrfanView <br> [Photopia](https://photopia.nl/) [Prop] <br> Windows Fax and Image viewer | [Gwenview](https://apps.kde.org/gwenview/) [KDE] <br> [Eye of GNOME](https://wiki.gnome.org/Apps/EyeOfGnome) (GNOME) <br> [gThumb](https://wiki.gnome.org/Apps/gthumb/) (GNOME) <br> [Mirage](https://mirageiv.sourceforge.net/) (GNOME) <br> [Geeqie](https://geeqie.sourceforge.net/) (GNOME) <br> [Xnview](https://www.xnview.com/){:.mp} <br> [Qiv](https://spiegl.de/qiv/) <br> Kview <br> [pornview](https://sourceforge.net/projects/pornview/) <br> ShowImg <br> Fbi <br> [Gthumb](https://wiki.gnome.org/Apps/gthumb) <br> PixiePlus<br> |
| Organizace fotografií | Zoner Photo Studio <br> Adobe Photoshop Lightroom <br> ACDSee<br> | [digiKam](https://www.digikam.org/){:.mp} <br> [F-Spot](https://github.com/f-spot/f-spot) <br> [Shotwell](https://wiki.gnome.org/Apps/Shotwell) <br> [gThumb](https://wiki.gnome.org/Apps/gthumb) <br> |
| Jednoduché grafické editory | Malování | [KolourPaint](https://apps.kde.org/kolourpaint/) (KDE) <br> [GNOME Paint](https://launchpad.net/gnome-paint) (GNOME) <br> [Tuxpaint](https://tuxpaint.org/){:.mp} <br> [Xpaint](https://sf-xpaint.sourceforge.net/) <br> [Gpaint](https://www.gnu.org/software/gpaint/) <br> [Graphtool](https://savannah.nongnu.org/projects/graphtool/) <br> Kpaint<br> |
| Pokročilé grafické editory | [GIMP](https://www.gimp.org/){:.mp} <br> Adobe Photoshop [Prop] <br> Paint Shop Pro [Prop] <br> Corel PhotoPaint [Prop] <br> Macromedia Fireworks <br> Zoner Photo Studio <br> Adobe Photoshop Lightroom <br> ACDSee <br> Corel PaintShop <br> Corel AfterShot <br> [Photopea](https://www.photopea.com/) [Prop] (web) | [GIMP](https://www.gimp.org/){:.mp} <br> [Krita](https://krita.org/){:.mp} (KDE) <br> [Pinta](https://www.pinta-project.com/) (GNOME) <br> [ImageMagick](https://imagemagick.org/) <br> [CinePaint](https://sourceforge.net/projects/cinepaint/) <br> [MyPaint](http://mypaint.intilinux.com/){:.mp} <br> [Darktable](https://www.darktable.org/) <br> [RawTherapee](https://www.rawtherapee.com/){:.mp} <br> [UFRaw](https://ufraw.sourceforge.net/) <br> [Photivo](https://photivo.org/) <br> Corel AfterShot [Prop] <br> [Photopea](https://www.photopea.com/) [Prop] (web) <br> [Fotoxx](https://kornelix.net/fotoxx/fotoxx.html) |
| Práce s vektorovou grafikou (+SVG) | [Inkscape](https://inkscape.org/){:.mp} <br> Adobe Illustrator <br> Corel Draw <br> Freehand <br> AutoSketch <br> [OpenOffice Draw](https://www.openoffice.org/cs/){:.mp} | [Inkscape](https://inkscape.org/){:.mp} <br> [Sodipodi](https://sourceforge.net/projects/sodipodi/){:.mp} <br> [Skencil](https://www.skencil.org/) <br> [Karbon](https://calligra.org/karbon/){:.mp} <br> [LibreOffice Draw](https://cs.libreoffice.org/){:.mp} <br> [OpenOffice Draw](https://www.openoffice.org/cs/){:.mp} <br> [Dia](https://wiki.gnome.org/Apps/Dia/) <br> [ImPress](https://www.ntlug.org/~ccox/impress/) <br> [Tgif](https://bourbon.usc.edu/tgif/) <br> Corel Draw 9 [Prop] |
| Snímání, nahrávání obrazovky | FastStone Capture <br> SnagIt <br> Fraps <br> CamStudio <br> Camtasia Studio<br> | [Shutter](https://shutter-project.org/) <br> [recordMyDesktop](https://recordmydesktop.sourceforge.net/) <br> [Kazam](https://launchpad.net/kazam) <br> Istanbul <br> Byzanz <br> KSnapshot (standard KDE) <br> gnome-screenshot (standard GNOME) <br> scrot (konsole) <br> |
{:.table .table-bordered .table-hover}

### Přehrávání a zpracování videa
{:#video}

| Popis programu | MS Windows | GNU/Linux |
| --- |
| Video a DVD přehrávače | Windows Media Player <br> BSplayer <br> [VLC Media Player](https://www.videolan.org/){:.mp} <br> Winamp [Mplayer](http://www.mplayerhq.hu/){:.mp} <br> RealPlayer <br> PowerDVD  | [VLC Media Player](https://www.videolan.org/){:.mp} <br> [Mplayer](http://www.mplayerhq.hu/){:.mp} <br> [mpv](https://mpv.io/){:.mp} <br> [Totem](https://wiki.gnome.org/Apps/Videos/) <br> [Kaffeine](https://www.kde.org/applications/multimedia/kaffeine/) <br> [Dragon Player](https://www.kde.org/applications/multimedia/dragonplayer/) <br> [Miro](https://www.getmiro.com/){:.mp} |
| Nástroje na úpravu videa | Windows Movie Maker <br> Adobe Premiere <br> Media Studio Pro | [OpenShot](https://www.openshot.org/){:.mp} <br> [Pitivi](https://www.pitivi.org/) <br> [Kdenlive](https://kdenlive.org/) <br> [Avidemux](https://avidemux.sourceforge.net/){:.mp} <br> [Shotcut](https://www.shotcut.org/){:.mp} <br> [Lightworks](https://www.lwks.com/){:.mp} <br> [Jahsaka](https://www.jahshaka.com/) |
| Konverze videa | Virtual Dub <br> [Mencoder](http://www.mplayerhq.hu/){:.mp} (z Mplayeru) | [Mencoder](http://www.mplayerhq.hu/){:.mp} (z Mplayeru) <br> [Ffmpeg](https://ffmpeg.org/) <br> [HandBrake](https://handbrake.fr/){:.mp} <br> [Mobile Media Converter](https://www.miksoft.net/mobileMediaConverter.php) |
| Multimediální centra | Windows Media Center <br> [Kodi](https://kodi.tv/){:.mp} <br> [MythTV](https://www.mythtv.org/){:.mp} | [Kodi (dříve XBMC Media Center)](https://kodi.tv/){:.mp} <br> [MythTV](https://www.mythtv.org/){:.mp} |
{:.table .table-bordered .table-hover}

### Programování a vývoj
{:#programovani}

| Popis programu | MS Windows | GNU/Linux |
| --- |
| IDE | Většina je multiplatformních nebo má mezi vývojáři dobře známé alternativy, které multiplatformní jsou. |
| Víceúčelové textové editory | PSPad <br> Notepad++ <br> [jEdit](http://www.jedit.org/){:.mp} | [Bluefish](https://bluefish.openoffice.nl/){:.mp} <br> [Geany](https://www.geany.org/){:.mp} <br> [Kate](https://kate-editor.org/) <br> [Gedit](https://wiki.gnome.org/Apps/Gedit) <br> [Vim](https://www.vim.org/) <br> [Emacs](https://www.gnu.org/software/emacs/) <br> [Scribes](https://scribes.sourceforge.net/) <br> Mcedit (z [mc](https://midnight-commander.org/)) <br> [jEdit](http://www.jedit.org/){:.mp} |
| HEX-editor | HxD <br> PSPad <br> Notepad++ (+ plugin) <br> Hex Editor <br> [Cheat Engine](https://cheatengine.org/){:.mp} | [Cheat Engine](https://cheatengine.org/){:.mp} <br> [hexedit](https://hexedit.sourceforge.net/) (konsole) <br> [Emacs](https://www.gnu.org/software/emacs/) <br> [GHex](https://directory.fsf.org/wiki/Ghex) |
| Platforma dot-NET | MS .NET | [Mono](https://www.mono-project.com/) <br> [DotGNU](https://www.gnu.org/projects/dotgnu/) |
| Práce s verzovacími systémy | GitHub Desktop <br> TortoiseGit <br> [TortoiseHg](https://www.mercurial-scm.org/wiki/TortoiseHg){:.mp} <br> TortoiseSVN | [Git](https://git-scm.com/){:.mp} (konsole) <br> [Mercurial](https://www.mercurial-scm.org/){:.mp} (konsole) <br> [TortoiseHg](https://www.mercurial-scm.org/wiki/TortoiseHg){:.mp} <br> [Subversion](https://subversion.apache.org/){:.mp} (konsole) <br> [Sourcetree](https://www.sourcetreeapp.com/) [Prop] |
{:.table .table-bordered .table-hover}

### Různé nástroje
{:#nastroje}

| Popis programu | MS Windows | GNU/Linux |
| --- |
| Šifrování | [OpenPGP](https://www.openpgp.org/) <br> [GnuPG](https://www.gnupg.org/) | [OpenPGP](https://www.openpgp.org/) <br> [GnuPG](https://www.gnupg.org/) <br> [KGpg](https://utils.kde.org/projects/kgpg/) |
| Rozpoznávání textu (OCR) | Recognita <br> FineReader | [Clara OCR](https://github.com/thequux/Clara-OCR) <br> [Gocr](https://jocr.sourceforge.net/) <br> [Kooka](https://userbase.kde.org/Kooka) |
| Skenování | [VueScan](https://www.hamrick.com/){:.mp} <br> OCR nástroje dodávané k tiskárnám | [Xsane](https://gitlab.com/sane-project/frontend/xsane) <br> [Simple Scan](https://launchpad.net/simple-scan) (GNOME) <br> [VueScan](https://www.hamrick.com/){:.mp} [Prop] |
| Antiviry | AVG <br> Avast <br> ESET <br> Kaspersky | běžně není potřeba <br> [ClamAV](https://www.clamav.net/) |
| Zálohování dat | zálohování Windows <br> [CrashPlan](https://www.crashplan.com/){:.mp} | [Déjà Dup](https://wiki.gnome.org/Apps/DejaDup) <br> [KBackup](https://www.kde.org/applications/utilities/kbackup/) <br> [CrashPlan](https://www.crashplan.com/){:.mp} [Prop] |
{:.table .table-bordered .table-hover}

### Emulátory a virtuální počítače
{:#emulatory}

| Popis programu | MS Windows | GNU/Linux |
| --- |
| Virtualizační nástroje | [Oracle VM VirtualBox](https://www.virtualbox.org/){:.mp} <br> [VMWare](https://www.vmware.com/){:.mp} | [Oracle VM VirtualBox](https://www.virtualbox.org/){:.mp} <br> [VMWare](https://www.vmware.com/){:.mp} [Prop] <br> [KVM](https://www.linux-kvm.org/) <br> [QEMU](https://www.qemu.org/){:.mp} <br> [Gnome Boxy](https://help.gnome.org/users/gnome-boxes/stable/) |
| Windows emulátor | - | [Wine](https://www.winehq.org/) <br> [PlayOnLinux](https://www.playonlinux.com/) <br> [Lutris](https://lutris.net/) |
| DOS emulátor | [DOSBox](https://www.dosbox.com/){:.mp} | [DOSBox](https://www.dosbox.com/){:.mp} |
| Sony PlayStation emulátor | [ePSXe](https://www.epsxe.com/) | [ePSXe](https://www.epsxe.com/) <br> [PCSX2](https://pcsx2.net/) |
| ZX Spectrum emulátor | [seznam](https://www.worldofspectrum.org/emulators.html) | [seznam](https://www.worldofspectrum.org/emulators.html) |
| Atari ST emulátor | [Steem SSE](https://steemsse.sourceforge.io/) | [Steem SSE](https://steemsse.sourceforge.io/) <br> [StonX](https://stonx.sourceforge.net/) |
| Amiga emulátor | [WinUAE](https://www.winuae.net/) | [FS-UAE](https://fs-uae.net/){:.mp} |
| Game Boy emulátor | - | [VGBA](https://fms.komkon.org/VGBA/) |
| Atari 2600 Video Computer System emulátor | [Stella](https://stella-emu.github.io/) | [Stella](https://stella-emu.github.io/) |
| Wii U emulátor | [Cemu](https://cemu.info/) | - |
{:.table .table-bordered .table-hover}

### Jednoduché hry
{:#jednoduchehry}

| Popis programu | MS Windows | GNU/Linux |
| --- |
| Součástí OS | Minesweeper (Miny) <br> Solitaire <br> Srdce <br> Freecell <br> Spider <br> Pinball | [GNOME Games](https://wiki.gnome.org/Apps/Games) (2048, AisleRiot/Solitaire, Miny, Roboti, Sudoku, ...) <br> [KDE Games](https://games.kde.org/) |
{:.table .table-bordered .table-hover}


- [Prop]      ... proprietární
- (KDE)       ... vhodné pro prostředí KDE
- (GNOME)     ... vhodné pro prostředí GNOME
- (konsole)   ... textové rozhraní

## Kde hledat další aplikace
{:#dalsi}

Kromě využití běžného vyhledávače lze použít i specializované katalogy programů pro GNU/Linux. Např. [FSF/UNESCO Free software directory](https://directory.fsf.org/), [GitHub](https://github.com/), [Launchpad](https://launchpad.net/), [SourceForge](https://sourceforge.net/), [Open Source Alternatives](https://www.osalt.com/) nebo [AlternativeTo](https://alternativeto.net/), případně samozřejmě české [AbcLinuxu](https://www.abclinuxu.cz/software).

Než začnete vyhledávat aplikaci přímo na jejích stránkách, přesvědčte se pomocí správce softwaru nebo instalačních nástrojů (apt, yum, dnf, urpmi, yast), že již není připravena přímo ve vaší distribuci.

## Převod dat některých běžných aplikací

### z MS Windows do Linuxu

Při přechodu na Linux můžete snadno převést data profilů některých aplikací. Převod je nejsnadnější u aplikací, které jsou tzv. multiplatformní. To znamená, že fungují na více operačních systémech. Také si u nich nemusíte zvykat na nové uživatelské rozhraní nebo hledat potřebné funkce. Které to jsou můžete zjistit v [tabulce ekvivalentních aplikací]({% link _pages/ekvivalenty.md %}).

Příkladem takových multiplatformních aplikací je webový prohlížeč Mozilla Firefox nebo poštovní klient Mozilla Thunderbird. Odkazy na návody k přenosu jejich dat uvádíme níže.

- [Mozilla Firefox](https://support.mozilla.org/cs/kb/jak-zazalohovat-obnovit-profil) (můžete také použít [Firefox Sync](https://www.mozilla.org/cs/firefox/sync/))
- [Mozilla Thunderbird](https://support.mozilla.org/cs/kb/profily-thunderbirdu) (pokud používáte [IMAP](https://support.mozilla.org/cs/kb/synchronizace-pres-protokol-imap), pro přenos pošty se stačí na Linuxu přihlásit ke své schránce)
